//chrome.storage.local.get(console.log)

// Pre-hide any pre-snoozed todos
chrome.storage.local.get({
    snoozed:[] 
}, function(data, snoozed_list) {
    var todos = document.getElementsByClassName("todo-pending");
    var toRemove = [];
    for (var i = 0; i < todos.length; i++) {
        var todoId = todos[i].id;
        for (var j=0; j < data.snoozed.length; j++) {
            var snoozedId = data.snoozed[j].id;
            var snoozedAt = data.snoozed[j].when;
            
            // console.log("Comparing " + todoId + " and " + snoozedId)
            if (todoId === snoozedId) {
                // console.log("Considering hiding " + todoId)
                // if when is between now and 24-hours ago
                var hoursToSnooze = 24;
                if (snoozedAt > (Date.now() - (1000 * 60 * 60 * hoursToSnooze))) {
                    // console.log("Hiding " + todoId)
                    toRemove.push(todos[i]);
                    continue;
                } else {
                    // it's expired. clear from storage
                    // console.log("Snooze expired for " + todoId)
                    
                    // to remove from storage, need to remove from the array
                    // and readd the array under the same key
                }
            }
        }
    }
    for (var i = 0; i < toRemove.length; i++) {
        toRemove[i].remove();
    }
});


function save_snooze(id) {
    chrome.storage.local.get({
        snoozed:[] //put defaultvalues if any
    }, function(data) {
        console.log(data.snoozed);
        var foo = {}
        foo.id = id
        foo.when = Date.now()

        data.snoozed.push(foo);
        
        chrome.storage.local.set({
            snoozed:data.snoozed
        }, function() {
            console.log("added to list with new values");
            let now = Date.now();
            var data = {};
            data[id] = now;
            chrome.storage.local.set({
                data
            }, function() {
                console.log("Snoozed " + id + " at " + now)
            });
        });
    });
}


var todos = document.getElementsByClassName('gl-flex-direction-row');
for (var i = 0; i < todos.length; i++ ) {

    var div = document.createElement('div');
    div.className = 'todo-actions gl-ml-3';

    var btn = document.createElement('a');
    btn.className = 'gl-button btn btn-default btn-loading d-flex align-items-center js-done-todo';
    btn.innerText = 'Snooze';
    btn.addEventListener('click', snooze, false);

    div.append(btn);
    todos[i].append(div)
}

function snooze(ev) {
    let elem = ev.target.parentElement.parentElement.parentElement;
    console.log("Want to snooze " + elem.id);

    elem.remove();
    save_snooze(elem.id)
}